import { useState, useEffect } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'//import boostrap
import NavBar from '../components/NavBar'; 
import { UserProvider } from '../contexts/UserContext.js'; 
import AppHelper from '../app-helper.js'; 
import { Container } from 'react-bootstrap'; 

function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState({email: null})

  useEffect(() => {
  	if(AppHelper.getAccessToken() !== null) {
  		const options = {
  			headers: { Authorization: `Bearer ${AppHelper.getAccessToken()}` }
  		}
  		fetch('https://budget-tracker-av.herokuapp.com/api/users/details', options).then((response) => response.json()).then(userData => {
  			if(typeof userData.email !== "undefined") {
  				setUser({ email: userData.email })
  			} else {
  				setUser({ email: null })
  			}
  		})
  	}
  }, [user.id])

  const unsetUser = () => {
  	localStorage.clear()
  	setUser({ email: null })
  }

  return(
  	<div>
  	<UserProvider value={ {user, setUser, unsetUser} }>
		  	<NavBar />
		    <Container>
		  	  <Component {...pageProps} />
		  	</Container>
	  </UserProvider>
  	</div>
  	)
}

export default MyApp

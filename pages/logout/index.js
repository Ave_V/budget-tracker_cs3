import {  useEffect, Fragment, useContext } from 'react'
import Router from 'next/router'
import View from '../../components/View'
import UserContext from '../../contexts/UserContext.js'

export default () => {
  	const { unsetUser } = useContext(UserContext)

  	useEffect(() => {
  		unsetUser()

  		Router.push('/')
  	}, [] )

    return (
        <View title="Logout">
            <h5 className="text-center">Logging out...</h5>
            <h6 className="text-center">You will be redirected back to the login page shortly.</h6>   
        </View>
       
    )
}

//the goal of this module is to shorten written code in various parts of our project

module.exports = {
	getAccessToken: () => localStorage.getItem('token')
}